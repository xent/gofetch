package main

import (
	"fmt"
	"log"
	"os/exec"
)

func main() {
	os, err := exec.Command("uname").Output()
	if err != nil {
		log.Fatal(err)
	}
	version, err := exec.Command("uname", "-r").Output()
	if err != nil {
		log.Fatal(err)
	}
	libc, err := exec.Command("sh", "-c", "ldd --version | sed -n 1p").Output()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("OS: ", string(os))
	fmt.Println("kernel version: ", string(version))
	fmt.Println("libc: ", string(libc))
}
